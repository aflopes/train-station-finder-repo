Scenario: 2 characters and 2 stations
 
Given a list of stations 'DARTFORD,DARTMOUTH,TOWER HILL,DERBY'
When input 'DART'
Then should return the characters 'F,M' and the stations 'DARTFORD,DARTMOUTH'

Scenario: space character and 2 stations

Given a list of stations 'LIVERPOOL,LIVERPOOL LIME STREET,PADDINGTON'
When input 'LIVERPOOL'
Then should return the characters ' ' and the stations 'LIVERPOOL,LIVERPOOL LIME STREET'

Scenario: no characters and no stations

Given a list of stations 'EUSTON,LONDON BRIDGE,VICTORIA'
When input 'KINGS CROSS'
Then the application will return no next characters and no stations