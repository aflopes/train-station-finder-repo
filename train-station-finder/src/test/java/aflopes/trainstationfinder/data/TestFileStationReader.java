package aflopes.trainstationfinder.data;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TestFileStationReader {

    @Autowired
    FileStationReader reader;

    @Test
    public void testGetStations() throws IOException {
	List<String> stations = reader.getTrainStations();
	Assert.assertEquals(2, stations.size());
	Assert.assertEquals("station_1", stations.get(0));
	Assert.assertEquals("station_2", stations.get(1));
    }
}

