package aflopes.trainstationfinder.find;

import java.util.List;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class FindStationSteps.
 */
public class FindStationSteps {

    /** The finder. */
    @Autowired
    StationFinder finder;

    /** The stations. */
    List<String> stations;

    /** The input. */
    String input;

    /**
     * Given input.
     * 
     * @param stations
     *            the stations
     */
    @Given("a list of stations '$stations'")
    public void givenInput(@Named("stations") List<String> stations) {
	this.stations = stations;
	this.finder.index(stations);
    }

    /**
     * When input.
     * 
     * @param input
     *            the input
     */
    @When("input '$value'")
    public void whenInput(@Named("value") String input) {
	this.input = input;
    }

    /**
     * Then evaluate result.
     * 
     * @param characters
     *            the characters
     * @param stations
     *            the stations
     */
    @Then("should return the characters '$chars' and the stations '$stations'")
    public void thenEvaluateResult(@Named("chars") List<Character> characters,
	    @Named("stations") List<String> stations) {

	StationFinderResult result = finder.find(input);
	// Next Chars
	Assert.assertEquals(characters.size(), result.getNextCharacters().size());
	for (char c : characters) {
	    Assert.assertTrue(result.getNextCharacters().contains(c));
	}
	// Stations
	Assert.assertEquals(stations.size(), result.getNextStations().size());
	for (String station : stations) {
	    Assert.assertTrue(result.getNextStations().contains(station));
	}
    }

    @Then("the application will return no next characters and no stations")
    public void thenEvaluateEmptyResult() {
	StationFinderResult result = finder.find(input);
	Assert.assertEquals(0, result.getNextCharacters().size());
	Assert.assertEquals(0, result.getNextStations().size());
    }
}
