package aflopes.trainstationfinder.find;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.spring.SpringApplicationContextFactory;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;

import aflopes.trainstationfinder.jbehave.CharacterListConverter;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class TestFindStationSteps extends JUnitStory {

    @Override
    public Configuration configuration() {
	return new MostUsefulConfiguration()
		.useParameterConverters(
			new ParameterConverters()
				.addConverters(new CharacterListConverter()))
		.useStoryLoader(new LoadFromClasspath(this.getClass()))
		.useStoryReporterBuilder(
			new StoryReporterBuilder().withDefaultFormats()
				.withFormats(Format.CONSOLE, Format.TXT));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
	return new SpringStepsFactory(configuration(), createContext());
    }

    private ApplicationContext createContext() {
	return new SpringApplicationContextFactory(
		"classpath:aflopes/trainstationfinder/find/TestFindStationSteps-context.xml")
		.createApplicationContext();
    }
}