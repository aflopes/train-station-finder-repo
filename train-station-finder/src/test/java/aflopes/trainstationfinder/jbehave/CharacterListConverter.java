package aflopes.trainstationfinder.jbehave;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.junit.Assert;

/**
 * The Class CharacterListConverter.
 */
public class CharacterListConverter implements ParameterConverter {

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.jbehave.core.steps.ParameterConverters.ParameterConverter#accept(
     * java.lang.reflect.Type)
     */
    public boolean accept(Type type) {
	if (type instanceof ParameterizedType) {
	    Type rawType = rawType(type);
	    Type argumentType = argumentType(type);
	    return List.class.isAssignableFrom((Class<?>) rawType)
		    && Character.class
			    .isAssignableFrom((Class<?>) argumentType);
	}
	return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.jbehave.core.steps.ParameterConverters.ParameterConverter#convertValue
     * (java.lang.String, java.lang.reflect.Type)
     */
    public Object convertValue(String value, Type type) {
	List<String> values = Arrays.asList(value
		.split(ParameterConverters.DEFAULT_LIST_SEPARATOR));
	List<Character> characters = new ArrayList<Character>();
	for (String characterValue : values) {
	    Assert.assertEquals(1, characterValue.length());
	    characters.add(characterValue.charAt(0));
	}
	return characters;
    }

    /**
     * Raw type.
     * 
     * @param type
     *            the type
     * @return the type
     */
    private Type rawType(Type type) {
	return ((ParameterizedType) type).getRawType();
    }

    /**
     * Argument type.
     * 
     * @param type
     *            the type
     * @return the type
     */
    private Type argumentType(Type type) {
	return ((ParameterizedType) type).getActualTypeArguments()[0];
    }
}
