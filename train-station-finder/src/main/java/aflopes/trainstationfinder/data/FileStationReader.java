package aflopes.trainstationfinder.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class FileStationReader.
 */
public class FileStationReader implements StationsReader {

    /** The file resource path. */
    private String fileResourcePath;

    /**
     * Sets the file resource path.
     * 
     * @param fileResourcePath
     *            the new file resource path
     */
    public void setFileResourcePath(String fileResourcePath) {
	this.fileResourcePath = fileResourcePath;
    }

    /*
     * (non-Javadoc)
     * 
     * @see aflopes.trainstationfinder.data.StationsReader#getTrainStations()
     */
    public List<String> getTrainStations() throws IOException {
	List<String> stations = new ArrayList<String>();
	BufferedReader in = null;
	try {
	    in = new BufferedReader(new InputStreamReader(this
		    .getClass().getClassLoader()
		    .getResourceAsStream(fileResourcePath)));
	    String line = null;
	    while ((line = in.readLine()) != null) {
		stations.add(line);
	    }
	} finally {
	    if (in != null) {
		in.close();
	    }
	}
	return stations;
    }
}
