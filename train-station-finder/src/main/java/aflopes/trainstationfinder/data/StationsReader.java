package aflopes.trainstationfinder.data;

import java.io.IOException;
import java.util.List;

/**
 * The Interface TrainStationsReader.
 */
public interface StationsReader {

    /**
     * Gets the train stations.
     * 
     * @return the train stations
     * @throws IOException
     */
    public List<String> getTrainStations() throws IOException;
}
