package aflopes.trainstationfinder.data;

import java.util.Arrays;
import java.util.List;

/**
 * The Class StaticStationReader.
 */
public class StaticStationReader implements StationsReader {

    /*
     * (non-Javadoc)
     * 
     * @see aflopes.trainstationfinder.data.StationsReader#getTrainStations()
     */
    public List<String> getTrainStations() {
	return Arrays.asList("London Waterloo", "London Victoria",
		"London Bridge", "London Liverpool Street", "Clapham Junction",
		"London Euston", "London Charing Cross", "London Paddington",
		"Birmingham New Street", "London King's Cross",
		"Glasgow Central", "Leeds", "East Croydon",
		"London St Pancras", "Stratford", "Edinburgh Waverley",
		"Glasgow Queen Street", "Manchester Piccadilly",
		"London Cannon Street", "Wimbledon", "Reading", "Vauxhall",
		"Brighton", "London Fenchurch Street", "Gatwick Airport",
		"London Marylebone", "Liverpool Central",
		"Liverpool Lime Street", "London Blackfriars",
		"Highbury and Islington");
    }

}
