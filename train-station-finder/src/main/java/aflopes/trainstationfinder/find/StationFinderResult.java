package aflopes.trainstationfinder.find;

import java.util.ArrayList;
import java.util.List;


/**
 * The Class StationFinderResult.
 */
public class StationFinderResult {

    /** The next stations. */
    List<String> nextStations;

    /** The next characters. */
    List<Character> nextCharacters;

    /** The search time. */
    long searchTime;

    /**
     * Instantiates a new station finder result.
     */
    public StationFinderResult() {
	nextCharacters = new ArrayList<Character>();
	nextStations = new ArrayList<String>();
    }

    /**
     * Instantiates a new station finder result.
     * 
     * @param nextStations
     *            the next stations
     * @param nextCharacters
     *            the next characters
     */
    public StationFinderResult(List<String> nextStations,
	    List<Character> nextCharacters, long searchTime) {
	this.nextStations = nextStations;
	this.nextCharacters = nextCharacters;
	this.searchTime = searchTime;
    }

    /**
     * Gets the next characters.
     * 
     * @return the next characters
     */
    public List<Character> getNextCharacters() {
	return nextCharacters;
    }

    /**
     * Gets the next stations.
     * 
     * @return the next stations
     */
    public List<String> getNextStations() {
	return nextStations;
    }

    /**
     * Gets the search time.
     * 
     * @return the search time
     */
    public long getSearchTime() {
	return searchTime;
    }
}
