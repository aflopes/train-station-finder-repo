package aflopes.trainstationfinder.find;

import java.io.IOException;
import java.util.List;

import aflopes.trainstationfinder.data.StationsReader;

/**
 * The Class StationFinderService.
 */
public class StationFinderService {

    /** The finder. */
    private StationFinder finder;

    /** The reader. */
    private StationsReader reader;

    /**
     * Instantiates a new station finder service.
     * 
     * @param finder
     *            the finder
     * @param reader
     *            the reader
     */
    public StationFinderService(StationFinder finder, StationsReader reader) {
	this.finder = finder;
	this.reader = reader;
    }

    public void initializerFinder() throws IOException {
	finder.index(reader.getTrainStations());
    }

    /**
     * Find stations starting with.
     * 
     * @param name
     *            the name
     * @return the list
     */
    public StationFinderResult findStationsStartingWith(String name) {
	return finder.find(name);
    }

    /**
     * Index stations.
     * 
     * @param stations
     *            the stations
     */
    public void indexStations(List<String> stations) {
	finder.index(stations);
    }
}
