package aflopes.trainstationfinder.find;

import java.util.List;

/**
 * The Interface StationFinder.
 */
public interface StationFinder {

    /**
     * Index.
     * 
     * @param stations
     *            the stations
     */
    public void index(List<String> stations);


    /**
     * Find.
     * 
     * @param partialStationName
     *            the partial station name
     * @return the list
     */
    public StationFinderResult find(String partialStationName);

}
