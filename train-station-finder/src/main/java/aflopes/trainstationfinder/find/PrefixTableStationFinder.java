package aflopes.trainstationfinder.find;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Class PrefixTableStationFinder.
 */
public class PrefixTableStationFinder implements StationFinder {

    /** The table. */
    private Map<String, List<TableEntry>> table;

    /**
     * Instantiates a new prefix table station finder.
     */
    public PrefixTableStationFinder() {
	table = new HashMap<String, List<TableEntry>>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see aflopes.trainstationfinder.find.StationFinder#index(java.util.List)
     */
    public void index(List<String> stations) {
	for (String station : stations) {
	    String workString = new String();
	    for (int i = 0; i < station.length(); i++) {
		workString += String.valueOf(station.charAt(i));
		List<TableEntry> tableEntry = table.get(workString);
		if (tableEntry == null) {
		    tableEntry = new ArrayList<TableEntry>();
		    table.put(workString, tableEntry);
		}
		tableEntry.add(new TableEntry(station, i));
	    }
	}
    }


    /*
     * (non-Javadoc)
     * 
     * @see aflopes.trainstationfinder.find.StationFinder#find(java.lang.String)
     */
    public StationFinderResult find(String partialStationName) {
	long start = System.currentTimeMillis();
	List<TableEntry> results = table.get(partialStationName);
	if (results == null)
	    return new StationFinderResult();

	Set<Character> nextCharsSet = new HashSet<Character>();
	List<String> nextStations = new ArrayList<String>();

	for (int i = 0; i < results.size(); i++) {
	    nextStations.add(results.get(i).stationName);
	    Character nextChar = results.get(i).nextChar;
	    if (nextChar != null && !nextCharsSet.contains(nextChar))
		nextCharsSet.add(nextChar);
	}
	List<Character> nextChars = new ArrayList<Character>();
	nextChars.addAll(nextCharsSet);
	return new StationFinderResult(nextStations, nextChars,
		System.currentTimeMillis() - start);
    }

    /**
     * The Class TableEntry.
     */
    private class TableEntry {

	/** The next char. */
	final Character nextChar;

	/** The station name. */
	final String stationName;

	/**
	 * Instantiates a new table entry.
	 * 
	 * @param station
	 *            the station
	 * @param index
	 *            the index
	 */
	public TableEntry(String station, int index) {
	    this.stationName = station;
	    if (station.length() != index + 1) { // not the last character
		this.nextChar = station.charAt(index + 1);
	    } else {
		this.nextChar = null;
	    }
	}

    }
}
