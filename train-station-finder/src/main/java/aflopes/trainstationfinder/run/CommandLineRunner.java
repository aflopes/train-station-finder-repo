package aflopes.trainstationfinder.run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.util.StringUtils;

import aflopes.trainstationfinder.data.StationsReader;
import aflopes.trainstationfinder.find.StationFinder;
import aflopes.trainstationfinder.find.StationFinderResult;

/**
 * The Class CommandLineRunner.
 */
public class CommandLineRunner {

    @Autowired
    StationsReader reader;
    @Autowired
    StationFinder finder;

    private void run() throws IOException {
	BufferedReader in = null;
	try {
	    System.out.println("Loading stations...");
	    List<String> stations = reader.getTrainStations();
	    finder.index(stations);
	    System.out.println("Loaded the stations " + stations);
	    in = new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("Ready to search. To exit, enter \"quit\"!");
	    while (true) {
		System.out.print("Enter part of a station name: ");
		String stationName = in.readLine();

		if ("quit".equals(stationName))
		    break;

		StationFinderResult result = finder.find(stationName);
		System.out.println("\n["
			+ result.getSearchTime()
			+ "ms] For \""
			+ stationName
			+ "\" the possible next characters are ["
			+ StringUtils.collectionToCommaDelimitedString(result
				.getNextCharacters())
			+ "] for the possible stations ["
			+ StringUtils.collectionToCommaDelimitedString(result
				.getNextStations()) + "]");
	    }
	} finally {
	    if (in != null) {
		in.close();
	    }
	}
    }

    public static void main(String[] args) throws IOException {
	ApplicationContext ctx = new GenericXmlApplicationContext(
		CommandLineRunner.class, "context.xml");
	CommandLineRunner cmd = ctx.getBean("runner", CommandLineRunner.class);
	cmd.run();
	System.out.println("Goodbye!");
    }
}
